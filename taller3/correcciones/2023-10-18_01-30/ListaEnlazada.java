package aed;

import java.util.*;

public class ListaEnlazada<T> implements Secuencia<T> {

    private Nodo primer;
    private ListaEnlazada<T> seq;
    public Nodo ultimo;


    private class Nodo {
        private Nodo anterior; 
        private T elemento;
        private Nodo siguiente; 

        public Nodo (T elem, Nodo sig, Nodo ant){
            elemento = elem;
            siguiente = sig;
            anterior = ant;
        }
        public Nodo getSiguiente(){
            return siguiente;
        }
    }

    public ListaEnlazada() {
        primer = new Nodo(null, ultimo, null);
        ultimo = new Nodo(null,null,primer);
        primer.siguiente = ultimo;
    }

    public int longitud() {
        int cant = 0;
        Nodo actual = primer.siguiente;
        while(actual != ultimo){
            cant++;
            actual = actual.getSiguiente();
        }
        return cant;
    }

    public void agregarAdelante(T elem) {
        Nodo agregado = new Nodo(elem, ultimo, primer);
        primer.siguiente.anterior = agregado;
        agregado.siguiente = primer.siguiente;
        primer.siguiente = agregado;
    }

    public void agregarAtras(T elem) {
        Nodo agregado = new Nodo(elem, ultimo, primer);
        agregado.anterior = ultimo.anterior;
        ultimo.anterior.siguiente = agregado;
        ultimo.anterior = agregado;
        }

    public T obtener(int i) {
        int indice = 0;
        Nodo actual = primer.siguiente;
        while (i != indice){
            indice++;
            actual = actual.siguiente;
        };
        return actual.elemento;
    }

    public void eliminar(int i) {
        int indice = 0;
        Nodo actual = primer.siguiente;
        while(i != indice){
            actual = actual.siguiente;
            indice++;
        }
        if (primer.siguiente == ultimo){
            
        }
        
        else{  
        if(actual.siguiente != ultimo && actual.anterior != primer){
            actual.anterior.siguiente = actual.siguiente;
            actual.siguiente.anterior = actual.anterior;
        }
        else{
            if(i == 0){
              primer.siguiente = actual.siguiente;
              primer.siguiente.anterior = primer;
            }
            else{
                ultimo.anterior = actual.anterior;
                ultimo.anterior.siguiente = ultimo;
            }
        }
        }
        }

    public void modificarPosicion(int indice, T elem) {
        int i = 0;
        Nodo actual = primer.siguiente;
        Nodo viejo = null;
        while (i != indice){
            i++;
            actual = actual.siguiente;
        };
        if (i == 0){
            viejo = actual;
            actual = new Nodo(elem, actual.siguiente, primer);
            viejo.siguiente.anterior = actual;
            primer.siguiente = actual;
        }
        else{
            viejo = actual;
            actual = new Nodo(elem, actual.siguiente, actual.anterior);
            viejo.anterior.siguiente = actual; 
            viejo.siguiente.anterior = actual;
        }
    }

    public ListaEnlazada<T> copiar() {
        Nodo actual = primer.siguiente;
        seq = new ListaEnlazada<T>();
        while (actual != ultimo){
            seq.agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
        return seq;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        Nodo actual = lista.primer.siguiente;
        primer = new Nodo(null, ultimo, null);
        ultimo = new Nodo(null,null,primer);
        primer.siguiente = ultimo;
        while (actual != lista.ultimo){
            agregarAtras(actual.elemento);
            actual = actual.siguiente;
        }
    }
    
    @Override
    public String toString() {
        Nodo actual = primer.siguiente;
        String res = "[" + actual.elemento;
        actual = actual.siguiente;
        while (actual != ultimo){
            res  = res + ", "+ actual.elemento;
            actual = actual.siguiente;
        };
        return res + "]";
    }

    private class ListaIterador implements Iterador<T> {
    	Nodo actual = primer.siguiente;
        public boolean haySiguiente() {
            return actual != ultimo;
        }
        
        public boolean hayAnterior() {
	        return actual.anterior != primer; 
        }

        public T siguiente() {
	        actual = actual.siguiente;
            return actual.anterior.elemento;
        }
        
        public T anterior() {
            actual = actual.anterior;
            return actual.elemento;
        }
        }
        
        public Iterador<T> iterador() {
        Iterador<T> loop = new ListaIterador();
        return  loop;
    }
    }


