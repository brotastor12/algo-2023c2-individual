package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }
    public static void mergeSort(Router[] a, int n) {
        if (n < 2) {
            return;
        }
        int mid = n / 2;
        Router[] l = new Router[mid];
        Router[] r = new Router[n-mid];
    
        for (int i = 0; i < mid; i++) {
            l[i] = a[i];
        }
        for (int i = mid; i < n; i++) {
            r[i - mid] = a[i];
        }
        mergeSort(l, mid);
        mergeSort(r, n - mid);
    
        merge(a, l, r, mid, n - mid);
    }

    public static void merge(
        Router[] a, Router[] l, Router[] r, int left, int right) {
 
        int i = 0, j = 0, k = 0;
        while (i < left && j < right) {
            if (l[i].compareTo(r[j]) <= 0) {
                a[k++] = l[i++];
            }
            else {
                a[k++] = r[j++];
            }
        }
        while (i < left) {
            a[k++] = l[i++];
        }
        while (j < right) {
            a[k++] = r[j++];
        }
    }

    void insertionsort(Fragment[] frmnts)
    {
        int n = frmnts.length;
        for (int i = 1; i < n; ++i) {
            Fragment key = frmnts[i];
            int j = i - 1;
 
            while (j >= 0 && frmnts[j].compareTo(key) > 0) {
                frmnts[j + 1] = frmnts[j];
                j = j - 1;
            }
            frmnts[j + 1] = key;
        }
    }

    void insertionsortipv(String[] ipv4)
    {
        int n = ipv4.length;
        for (int i = 1; i < n; ++i) {
            String key = ipv4[i];
            int j = i - 1;
 
            while (j >= 0 && compareIPv4(key, ipv4[j]) == ipv4[j]) {
                ipv4[j + 1] = ipv4[j];
                j = j - 1;
            }
            ipv4[j + 1] = key;
        }
    }


    public Fragment[] tcpReorder(Fragment[] fragments) {
        insertionsort(fragments);
        return fragments;
    }
    
    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        Router[] res = new Router[k];
        int i = 0;
        int j = routers.length-1;
        mergeSort(routers, routers.length);
        while(i < res.length && j < routers.length){
            if (routers[j].getTrafico() < umbral){
                return res;
            }
            res[i] = routers[j];
            i++;
            j--;
        }
        return res;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {
        insertionsortipv(ipv4);
        IPv4Address[] res = new IPv4Address[ipv4.length];
        for(int i = 0; i < ipv4.length; i++){
            res[i] = new IPv4Address(ipv4[i]);
        }
        return res;
    }

    public String compareIPv4(String s1, String s2){
        IPv4Address a = new IPv4Address(s1);
        IPv4Address b = new IPv4Address(s2);
        int i = 0;
        while (i < 4){
            if (a.getOctet(i) > b.getOctet(i)){
                return s1;
            } else if(a.getOctet(i) < b.getOctet(i)){
                return s2;
            }
            i++;
        }
        return s1;
    }


}
