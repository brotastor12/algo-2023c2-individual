package aed;

import java.util.Scanner;
import java.io.IOException;
import java.io.PrintStream;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] xs= new float[largo];
        for (int i = 0; i < largo; i++){
            xs[i] = entrada.nextFloat();
        };
        return xs;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] xy = new float[filas][];
        if (filas == 0 || columnas == 0){
            return new float[0][0];
        }
        for (int i = 0; i < filas; i++){
            xy[i] = leerVector(entrada, columnas);
            entrada.nextLine();
        }
        return xy;
    }

    void imprimirPiramide(PrintStream salida, int alto){ 
        int largo = 1;
        int k = 1;
          for (;largo <= alto*2; largo+=2){
                    int escritos = 0;
                for (int i = 0;i<alto*2-1;i++){
                    if (i>= alto-k && escritos < largo){
                     try {
                        salida.write(new byte[]{'*'});
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                        escritos +=1;
                  }
                    else{
                       try {
                        salida.write(new byte[]{' '});
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                 }
              }
             try {
                salida.write(new byte[]{'\n'});
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
             k += 1;
         }
        }
    }   

