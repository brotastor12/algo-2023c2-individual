package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo raiz;
    private int cardinal;


    private class Nodo {
        private Nodo izq;
        private Nodo der;
        private Nodo padre;
        private T elemento;

        public Nodo(T elem, Nodo hmen, Nodo hmay, Nodo pad){
            izq = hmen;
            der = hmay;
            padre = pad;
            elemento = elem;
        }
        public Nodo getizq(){
            return izq;
        }
        public Nodo getder(){
            return der;
        }
        public Nodo getpad(){
            return padre;
        }
        public T getelem(){
            return elemento;
        }
    }

    public ABB() {
        raiz = null;
        cardinal = 0;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        Nodo actual = raiz;
        while(actual.getizq() != null){
            actual = actual.getizq();
        }
        return actual.getelem();
    }

    public T maximo(){
        Nodo actual = raiz;
        while(actual.getder() != null){
            actual = actual.getder();
        }
        return actual.getelem();
    }

    public void insertar(T elem){
        Nodo actual = raiz;
        Nodo nuevo = new Nodo(elem, null, null, null);
        Nodo nuevoPadre = nuevo;
        if (raiz == null){
            raiz = nuevo;
            cardinal +=1;
        }
        while (actual != null) {
            if(actual.getelem().compareTo(elem) < 0){
                nuevoPadre = actual;
                actual = actual.der;
            }else if (actual.getelem().compareTo(elem) > 0){
                nuevoPadre = actual;
                actual = actual.izq;
             }
            else if (actual.getelem().compareTo(elem) == 0) {
                 nuevoPadre = nuevo;
                 actual = null;
            }
         }
         if (nuevoPadre != nuevo && nuevoPadre.getelem().compareTo(elem) < 0){
            nuevo.padre = nuevoPadre;
            nuevoPadre.der = nuevo;
            cardinal += 1;
         } else if(nuevoPadre != nuevo && nuevoPadre.getelem().compareTo(elem) > 0){
            nuevoPadre.izq = nuevo;
            nuevo.padre = nuevoPadre;
            cardinal +=1;
         }
    }
    public boolean pertenece(T elem){
        if (raiz == null) {
            return false;
        }
        if (elem == raiz.getelem()) {
            return true;
        } 
        Nodo nodo = buscar(elem);
        if (nodo == null){
            return false;
        } else {
            return true;
        }
    }
    private Nodo buscar(T elem){
        Nodo actual = raiz;

        while(actual != null && elem.compareTo(actual.getelem()) != 0) {
            if (elem.compareTo(actual.getelem()) < 0) {
                actual = actual.getizq();
            } else {
                actual = actual.getder();
            }
        }
        return actual;
    }


    private Nodo sucesor(Nodo nodo){
        Nodo actual = nodo.der;
        while(actual.izq != null){
            actual = actual.izq;
        }
        return actual;
    }

    public void eliminar(T elem){
        if (!pertenece(elem)) {
            return;
        }

        cardinal -= 1;
        Nodo nodo = buscar(elem);
        Nodo padre = nodo.padre;

        if (nodo.izq != null && nodo.der != null) {
            Nodo sucesor = sucesor(nodo);
            nodo.elemento = sucesor.elemento;
            nodo = sucesor;
            padre = nodo.padre;
        }

        if (nodo.izq == null && nodo.der == null) {
            if (padre == null) {
                raiz = null;
            } else if (padre.elemento.compareTo(nodo.elemento) > 0){
                padre.izq = null;
            } else if (padre.elemento.compareTo(nodo.elemento) <= 0) {
                padre.der = null;
            }

        } else if (nodo.izq != null || nodo.der != null) {
            if (nodo.der == null) {
                nodo.izq.padre = padre;
                if (padre == null) {
                    raiz = nodo.izq;
                } else if (padre.elemento.compareTo(elem) > 0){
                    padre.izq = nodo.izq;
                } else if (padre.elemento.compareTo(elem) < 0) {
                    padre.der = nodo.izq;
                }

            } else if (nodo.izq == null) {
                nodo.der.padre = padre;
                if (padre == null) {
                    raiz = nodo.der;
                } else if (padre.elemento.compareTo(nodo.elemento) > 0) {
                    padre.izq = nodo.der;
                } else if (padre.elemento.compareTo(nodo.elemento) < 0) {
                    padre.der = nodo.der;
                } else if (padre.elemento.compareTo(nodo.elemento) == 0) {
                    padre.der = nodo.der;
                }
            }
        }    

    }
   

    public String toString(){
        Iterador<T> loop = iterador();
        String res = "{" + loop.siguiente();
        while(loop.haySiguiente()){
            res += "," + loop.siguiente();
        }
        return res += "}";
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;
        private T min;
        private T max;
        private Nodo primer;
        
        public boolean haySiguiente(){
            return _actual.getelem() != max;
        }
    
        public T siguiente() {
            T elemactual = _actual.getelem();
                if (_actual == primer){
                    _actual = primer.getpad();
                    return _actual.getelem();
                }
                if (min == _actual.getelem()){
                    _actual = _actual.getpad();
                    return _actual.getelem();
                }
                if(_actual.getder() == null){
                    _actual = _actual.getpad();
                    if(_actual.getelem().compareTo(elemactual) > 0){
                        elemactual = _actual.getelem();
                        return elemactual;
                    }

                    while (elemactual.compareTo(_actual.getder().getelem()) >= 0){
                        _actual = _actual.getpad();
                    }
                    if(_actual.getder() != null){
                        if(elemactual.compareTo(_actual.getelem()) >= 0){
                            _actual = _actual.getder();
                            while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                            _actual = _actual.getizq();
                            }
                            elemactual = _actual.getelem();
                            return elemactual;

                        } else {
                            elemactual = _actual.getelem();
                            return elemactual;
                        }
                    }
                    return _actual.getelem();

                } else if(_actual.getder().getelem().compareTo(elemactual) > 0){
                    if(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                        while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                            _actual = _actual.getizq();
                        }
                        elemactual = _actual.getelem();
                        return elemactual;
                    } 
                    _actual = _actual.getder();
                    
                    while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                        _actual = _actual.getizq();
                    }
                    
                    elemactual = _actual.getelem();
                    return elemactual;
                
                } else{
                    while (elemactual.compareTo(_actual.getder().getelem()) < 0){
                        _actual = _actual.getpad();
                    }
                    elemactual = _actual.getelem();
                    return _actual.getelem();
                }
            }
        }

    public Iterador<T> iterador() {
        Nodo actual = raiz;
        while(actual.getizq() != null){
            actual = actual.getizq();
        }
        ABB_Iterador loop = new ABB_Iterador();
        loop._actual = actual;
        loop.primer = new Nodo(null, null, null, actual);
        loop._actual = loop.primer;
        loop.min = minimo();
        loop.max = maximo();
        return loop;
    }

}
