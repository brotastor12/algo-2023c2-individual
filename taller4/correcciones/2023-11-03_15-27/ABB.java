package aed;

import java.util.*;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo raiz;
    private int cardinal;


    private class Nodo {
        private Nodo izq;
        private Nodo der;
        private Nodo padre;
        private T elemento;

        public Nodo(T elem, Nodo hmen, Nodo hmay, Nodo pad){
            izq = hmen;
            der = hmay;
            padre = pad;
            elemento = elem;
        }
        public Nodo getizq(){
            return izq;
        }
        public Nodo getder(){
            return der;
        }
        public Nodo getpad(){
            return padre;
        }
        public T getelem(){
            return elemento;
        }
    }

    public ABB() {
        raiz = null;
        cardinal = 0;
    }

    public int cardinal() {
        return cardinal;
    }

    public T minimo(){
        Nodo actual = raiz;
        while(actual.getizq() != null){
            actual = actual.getizq();
        }
        return actual.getelem();
    }

    public T maximo(){
        Nodo actual = raiz;
        while(actual.getder() != null){
            actual = actual.getder();
        }
        return actual.getelem();
    }

    public void insertar(T elem){
        Nodo actual = raiz;
        Nodo nuevo = new Nodo(elem, null, null, null);
        Nodo nuevoPadre = nuevo;
        if (raiz == null){
            raiz = nuevo;
            cardinal +=1;
        }
        while (actual != null) {
            if(actual.getelem().compareTo(elem) < 0){
                nuevoPadre = actual;
                actual = actual.der;
            }else if (actual.getelem().compareTo(elem) > 0){
                nuevoPadre = actual;
                actual = actual.izq;
             }
            else if (actual.getelem().compareTo(elem) == 0) {
                 nuevoPadre = nuevo;
                 actual = null;
            }
         }
         if (nuevoPadre != nuevo && nuevoPadre.getelem().compareTo(elem) < 0){
            nuevo.padre = nuevoPadre;
            nuevoPadre.der = nuevo;
            cardinal += 1;
         } else if(nuevoPadre != nuevo && nuevoPadre.getelem().compareTo(elem) > 0){
            nuevoPadre.izq = nuevo;
            nuevo.padre = nuevoPadre;
            cardinal +=1;
         }
    }
    public boolean pertenece(T elem){
        Nodo actual = raiz;
        while(actual != null){
            if(actual.getelem().compareTo(elem) == 0){
                return true;
            } else if(actual.getelem().compareTo(elem) > 0){
                actual = actual.getizq();
            } else{
                actual = actual.getder();
            }
        }
        return false;
    }


    public void eliminar(T elem){
        cardinal -= 1;
        Nodo actual = raiz;
        Nodo reemp = null;
        while (actual != null) {
            if(actual.getelem().compareTo(elem) < 0){
                actual = actual.der;
            }else if (actual.getelem().compareTo(elem) > 0){
                actual = actual.izq;
             }
            else if (actual.getelem().compareTo(elem) == 0) {
                break;
            }
        }
        if (actual.getelem().compareTo(elem) == 0){
            if(actual == raiz && actual.getder() == null && actual.getizq() == null){
                raiz = null;
            }
            if (actual.getder() != null && actual.getizq() != null){
                reemp = actual.getder();
                while(reemp.getizq() != null){
                    reemp = reemp.getizq();
                }
                eliminar(reemp.getelem());
                cardinal++;
                actual.elemento = reemp.getelem(); 
            } else if(actual.getder() == null && actual.getizq() != null){
                reemp = actual.getizq();
                eliminar(reemp.getelem());
                cardinal++;
                actual.elemento = reemp.getelem();
            } else if(actual.getder() != null && actual.getizq() == null){
                reemp = actual.getder();
                eliminar(reemp.getelem());
                cardinal++;
                actual.elemento = reemp.getelem();
            } else if(actual.izq == null && actual.getder() == null){
                if (actual.getpad().getelem().compareTo(elem) > 0){
                    actual.padre.izq = null;
                    actual.padre = null;
                }else{
                    actual.padre.der = null;
                    actual.padre = null;
                }
            }
        }
    }

    public String toString(){
        Iterador<T> loop = iterador();
        String res = "{" + loop.siguiente();
        while(loop.haySiguiente()){
            res += "," + loop.siguiente();
        }
        return res += "}";
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;
        private T min;
        private T max;
        private Nodo primer;
        
        public boolean haySiguiente(){
            return _actual.getelem() != max;
        }
    
        public T siguiente() {
            T elemactual = _actual.getelem();
                if (_actual == primer){
                    _actual = primer.getpad();
                    return _actual.getelem();
                }
                if (min == _actual.getelem()){
                    _actual = _actual.getpad();
                    return _actual.getelem();
                }
                if(_actual.getder() == null){
                    _actual = _actual.getpad();
                    if(_actual.getelem().compareTo(elemactual) > 0){
                        elemactual = _actual.getelem();
                        return elemactual;
                    }

                    while (elemactual.compareTo(_actual.getder().getelem()) >= 0){
                        _actual = _actual.getpad();
                    }
                    if(_actual.getder() != null){
                        if(elemactual.compareTo(_actual.getelem()) >= 0){
                            _actual = _actual.getder();
                            while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                            _actual = _actual.getizq();
                            }
                            elemactual = _actual.getelem();
                            return elemactual;

                        } else {
                            elemactual = _actual.getelem();
                            return elemactual;
                        }
                    }
                    return _actual.getelem();

                } else if(_actual.getder().getelem().compareTo(elemactual) > 0){
                    if(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                        while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                            _actual = _actual.getizq();
                        }
                        elemactual = _actual.getelem();
                        return elemactual;
                    } 
                    _actual = _actual.getder();
                    
                    while(_actual.getizq() != null && _actual.getizq().getelem().compareTo(elemactual) > 0){
                        _actual = _actual.getizq();
                    }
                    
                    elemactual = _actual.getelem();
                    return elemactual;
                
                } else{
                    while (elemactual.compareTo(_actual.getder().getelem()) < 0){
                        _actual = _actual.getpad();
                    }
                    elemactual = _actual.getelem();
                    return _actual.getelem();
                }
            }
        }

    public Iterador<T> iterador() {
        Nodo actual = raiz;
        while(actual.getizq() != null){
            actual = actual.getizq();
        }
        ABB_Iterador loop = new ABB_Iterador();
        loop._actual = actual;
        loop.primer = new Nodo(null, null, null, actual);
        loop._actual = loop.primer;
        loop.min = minimo();
        loop.max = maximo();
        return loop;
    }

}
